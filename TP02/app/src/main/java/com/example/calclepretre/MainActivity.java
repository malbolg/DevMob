package com.example.calclepretre;

import static java.lang.Double.parseDouble;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText Value1;
    EditText Value2;

    RadioGroup OperatorSelector;

    TextView TextResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Value1 = (EditText) findViewById(R.id.value1);
        Value2 = (EditText) findViewById(R.id.value2);
        OperatorSelector = (RadioGroup) findViewById(R.id.RadioGroup);
        TextResult = (TextView) findViewById(R.id.resultatValue);
    }

    public void OnCalc(View unused) {
        String V1_str = Value1.getText().toString();
        String V2_str = Value2.getText().toString();

        if( V1_str.equals("") || V2_str.equals("") ) {
            TextResult.setText(R.string.invalid_number);
            return;
        }

        double V1 = parseDouble(V1_str);
        double V2 = parseDouble(V2_str);

        int operatorID = OperatorSelector.getCheckedRadioButtonId();
        double result = -1;

        if( operatorID == R.id.radioPlus ) {
            result = V1 + V2;
        } else if (operatorID == R.id.radioMinus ) {
            result = V1 - V2;
        } else if (operatorID == R.id.radioMul ) {
            result = V1 * V2;
        } else if (operatorID == R.id.radioDivide ) {
            result = V1 / V2;
        } else {
            TextResult.setText(R.string.invalid_operand);
            return;
        }

        TextResult.setText(String.valueOf(result));
    }
    
    public void OnRAZ(View unused) {
        Value1.setText("");
        Value2.setText("");
        TextResult.setText("");
    }
    
    public void OnQuit(View unused) {
        finish();
        System.exit(0);
    }
}