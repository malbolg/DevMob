package com.example.projetlepretre.helpers;

import android.annotation.SuppressLint;
import android.content.Context;

import com.example.projetlepretre.types.Question;
import com.example.projetlepretre.types.ScoreListSingleton;
import com.example.projetlepretre.types.ScoreStruct;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

// TODO:
// Rewrite categories file without using an objectInputStream
// Maybe we should keep the csv format for it? reading it is implemented and writing is fairly
// easy

/**
 * <p>Class that handles anything filestream-related</p>
 *
 * <p>the general structure is that we have a categories file that links categories to
 * their respective form file which contains the question.</p>
 *
 * <p>This allows us to use just about anything for category names, even emojis</p>
 */
public class RessourceHandler {
	public static final String
		SCORE_FILE_NAME="Score.dat",
		CATEGORIES_FILE_NAME="Index.lnks";


	/**
	 * <p>Method used to get a stream on a file in the res/raw folder.</p>
	 * <p>Useful for the initial loading of the default forms in the raw folder.</p>
	 *
	 * @param ctx      the context, usually <code>this</code>
	 * @param filename filename
	 * @return InputStream
	 */
	@SuppressLint("DiscouragedApi")
	private static InputStream getRawRessourceStream(Context ctx, final String filename) {
		return ctx.getResources().openRawResource(
				ctx.getResources().getIdentifier(filename,
						"raw", ctx.getPackageName()
				)
		);
	}

	/**
	 * Returns the mapping of the default categories to the default form files (all in raw folder).
	 * This is used to initialise the storage of the app with some example forms.
	 *
	 * @param ctx the context, usually <code>this</code>
	 * @return Map of category to filename
	 * @throws IOException if the <code>categories</code> file is missing from the raw folder
	 */
	private static Map<String, String> load_raw_links(Context ctx)
			throws IOException {
		InputStreamReader inputStreamReader = new InputStreamReader(
				getRawRessourceStream(ctx, "categories")
		);

		Map<String, String> links = RessourceParser.parse_links(inputStreamReader);

		inputStreamReader.close();
		return links;
	}

	/**
	 * Builds the list of all questions and their answers (the form) from a filename.
	 *
	 * @param ctx      the context, usually <code>this</code>
	 * @param filename the name of the storage file for the question and answers
	 * @return An arrayList containing all the questions
	 * @throws IOException if the file was not found
	 */
	public static ArrayList<Question> load_form(Context ctx, String filename)
			throws IOException {
		ObjectInputStream in_stream = new ObjectInputStream(
				ctx.openFileInput(filename)
		);

		try {
			//noinspection unchecked
			ArrayList<Question> form = (ArrayList<Question>) in_stream.readObject();
			in_stream.close();

			return form;
		} catch (ClassNotFoundException e) {
			in_stream.close();
			throw new RuntimeException("failed to parse app/categories.");
		}
	}


	/**
	 * <p>Writes a new user defined form to the application storage. This automatically
	 * performs a link in the app's <code>categories</code> file.</p>
	 * <p>if the <code>categories</code> file does not exists in the app storage, creates it.
	 * </p>
	 * @param ctx      the context, usually <code>this</code>
	 * @param category - The category of the created form @param questions - Its questions
	 */
	public static void save_form(Context ctx, String category, ArrayList<Question> questions) {
		@SuppressLint("SimpleDateFormat")
		SimpleDateFormat date = new SimpleDateFormat("yy-MM-dd-HH:mm:ss");

		// IE : 24/01/16-16:30:00
		// why? this automatically sort files chronologically if you sort by name and avoids name
		// collisions
		String dateString = date.format(new Date());
		String filename = dateString + "_0";

		// ensure we don't overwrite any files if we save multiple forms at once
		int i = 1;
		while (has_file(ctx, filename)) {
			filename = dateString + "_" + i;
			i++;
		}

		// perform linking
		try (ObjectOutputStream out_stream = new ObjectOutputStream(
				ctx.openFileOutput(filename, Context.MODE_PRIVATE)
		)) {
			out_stream.writeObject(questions);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		// write links to the categories file, we store as strings because
		// ObjectStreams do not support appending
		try (FileOutputStream out_stream =
					 ctx.openFileOutput(CATEGORIES_FILE_NAME, Context.MODE_APPEND)
		) {
			OutputStreamWriter writer = new OutputStreamWriter(out_stream);

			writer.write(
					RessourceUnparser.unparse_single_link(category, filename)
			);

			writer.write('\n');
			writer.close();
		} catch (IOException e) {
			// if we fail here, delete the created file
			ctx.deleteFile(dateString);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Load scores into the singleton class responsible for managing them. Will overwrite any
	 * scores contained within the class so make sure you save before running this again.
	 *
	 * @param ctx the context, usually <code>this</code>
	 */
	public static void load_scores(Context ctx) {
		ScoreListSingleton score_instance = ScoreListSingleton.getInstance();
		// in the scenario that someone calls this when the score are
		// already in memory
		score_instance.clear();

		try (FileInputStream fs = ctx.openFileInput(SCORE_FILE_NAME)
		) {
			ObjectInputStream input_stream = new ObjectInputStream(fs);

			//noinspection unchecked
			ArrayList<ScoreStruct> read_scores =
					(ArrayList<ScoreStruct>) input_stream.readObject();

			for (ScoreStruct scr : read_scores)
				score_instance.add(scr);

			input_stream.close();
		} catch (FileNotFoundException e) {
			// do nothing
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Parsing error while reading scores");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Saves the scores from the ScoreListSingleton class responsible of managing them.
	 *
	 * @param ctx the context, usually <code>categories</code>
	 */
	public static void write_scores(Context ctx) throws IOException {
		ObjectOutputStream outputStream = new ObjectOutputStream(
				ctx.openFileOutput(SCORE_FILE_NAME, Context.MODE_PRIVATE)
		);

		outputStream.writeObject(
				ScoreListSingleton.getInstance().getInternalList()
		);

		outputStream.close();
	}

	/** checks if the app storage has a certain file
	 *
	 */
	public static boolean has_file(Context ctx, String filename) {
		return Arrays.asList(ctx.fileList()).contains(filename);
	}

	public static Map<String, String> load_links(Context ctx) {
		try {
			Map<String, String> links;

			InputStreamReader is = new InputStreamReader(
					ctx.openFileInput(CATEGORIES_FILE_NAME)
			);

			links = RessourceParser.parse_links(is);

			is.close();
			return links;
		} catch(FileNotFoundException e) {
			throw new RuntimeException("Could not find app's categories file");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void import_default_forms(Context ctx) {
		try {
			Map<String, String> links = load_raw_links(ctx);

			// Read from the app's categories file the category and their associated files
			for(Map.Entry<String, String> pair : links.entrySet()) {
				String category = pair.getKey();
				String filename = pair.getValue();

				InputStreamReader question_reader = new InputStreamReader(
						getRawRessourceStream(ctx, filename)
				);

				ArrayList<Question> form = RessourceParser.parse_form_from_csv(question_reader);
				save_form(ctx, category, form);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
