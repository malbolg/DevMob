package com.example.projetlepretre.types;

import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/** Utlity class that represents the forms
 * it is used like an iterator */
public class FormIterator {

	// A form is a collection of questions
	private final ArrayList<Question> Form;
	private int question_index;

	public FormIterator(Context c, ArrayList<Question> questions)
			throws IOException {
		Form = questions;
		Collections.shuffle(Form);
		question_index = 0;
	}

	/**
	 * Iterate to the next question
	 * make sure to check if there is one with <code>has_next()</code>
	 * */
	public void next() {
		question_index += 1;
	}

	public boolean has_next() {
		return question_index < Form.size() -1;
	}

	/**
	 * Get count of questions
	 *
	 * @return int */
	public int count() { return Form.size();}

	public int current_index() {
		return question_index;
	}

	/**
	 * Get current question object
	 *
	 * @return Question
	 */
	public Question get_question() {
		return Form.get(question_index);
	}
}
