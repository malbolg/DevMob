package com.example.projetlepretre.helpers;

import java.util.Map;

/** Utility class that helps to unparse objects into string for easier writing into files */
public class RessourceUnparser {
	public static String unparse_links(Map<String, String> links) {
		StringBuilder str_builder = new StringBuilder();

		for (Map.Entry<String, String> pair : links.entrySet()) {
			String category = pair.getKey();
			String filename = pair.getValue();

			str_builder
					.append(category)
					.append(";")
					.append(filename)
					.append('\n');
		}

		// delete useless '\n' for easier parsing
		str_builder.deleteCharAt(str_builder.length() - 1);

		return str_builder.toString();
	}

	/** parse a link of a category to a file as a string */
	public static String unparse_single_link(String category, String value) {
		StringBuilder str_builder = new StringBuilder();

		return str_builder
				.append(category)
				.append(";")
				.append(value)
				.toString();
	}
}
