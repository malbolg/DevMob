package com.example.projetlepretre.types;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class Question implements Serializable {
    private String question;

    private ArrayList<String> answers;

    private ArrayList<Integer> right_answers_indexes;

    public Question() {
        answers = new ArrayList<>();
        right_answers_indexes = new ArrayList<>();
    }

    public void set_question(String q) {
        question = q;
    }

    public String get_header() {
        return question;
    }

    public ArrayList<String> get_possible_answers() {
        return answers;
    }

    public void add_answer(String ans, boolean required) {
        answers.add(ans);

        if (required)
            right_answers_indexes.add(answers.size() - 1);
    }

    /** Answer the question.
     *
     * @param selected_answers self-explainatory
     * @return true if correct, false otherwise
     * @noinspection unused
     */
    public boolean answer(ArrayList<Integer> selected_answers) {
        // select all correct answers
        // return selected_answers.containsAll(right_answers_indexes)
        //        && right_answers_indexes.containsAll(selected_answers);

        // select only one
        return right_answers_indexes.containsAll(selected_answers);
    }

    /** Answer a single-choice question.
     *
     * @param idx the index of the answer
     * @return false if incorrect or answering a multiple-choice question, true otherwise.
     */
    public boolean answer(int idx) {
        return right_answers_indexes.size() == 1 && right_answers_indexes.get(0) == idx;
    }

    public boolean has_multiple_choice() {
        return right_answers_indexes.size() > 1;
    }

    private void writeObject(java.io.ObjectOutputStream out_stream)
            throws IOException {
        out_stream.defaultWriteObject();

        out_stream.writeUTF(question);
        out_stream.writeObject(answers);
        out_stream.writeObject(right_answers_indexes);
    }

    /** @noinspection unchecked*/
    private void readObject(java.io.ObjectInputStream in_stream)
            throws IOException, ClassNotFoundException {
        in_stream.defaultReadObject();

        question = in_stream.readUTF();
        answers = (ArrayList<String>) in_stream.readObject();
        right_answers_indexes = (ArrayList<Integer>) in_stream.readObject();
    }
}
