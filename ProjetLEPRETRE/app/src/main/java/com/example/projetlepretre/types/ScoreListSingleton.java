package com.example.projetlepretre.types;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;

/** Utility class that helps sync scores across activities */
public class ScoreListSingleton implements Iterable<ScoreStruct> {
	private static ScoreListSingleton _instance;
	private final ArrayList<ScoreStruct> internal_list;

	private ScoreListSingleton() {
		_instance = this;
		internal_list = new ArrayList<>();
	}

	public static ScoreListSingleton getInstance() {
		if( _instance != null)
			return _instance;

		return new ScoreListSingleton();
	}

	public void clear() {
		internal_list.clear();
	}

	/** Did we do a form named <code>categories</code>?*/
	public boolean has(String category) {
		return internal_list.stream().anyMatch(scr -> scr.category.equals(category));
	}

	public boolean add(ScoreStruct scr) {
		return internal_list.add(scr);
	}

	public void removeIf(Predicate<? super ScoreStruct> func) {
		internal_list.removeIf(func);
	}

	public ArrayList<ScoreStruct> getInternalList() {
		return internal_list;
	}

	@NonNull
	@Override
	public Iterator<ScoreStruct> iterator() {
		return internal_list.iterator();
	}

	@Override
	public void forEach(@NonNull Consumer<? super ScoreStruct> action) {
		internal_list.forEach(action);
	}

	@NonNull
	@Override
	public Spliterator<ScoreStruct> spliterator() {
		return internal_list.spliterator();
	}
}
