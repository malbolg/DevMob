package com.example.projetlepretre.dialogs;

import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projetlepretre.R;

public class AdminLoginDialog {

	public static void show(AppCompatActivity activity, Runnable onSuccess) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		LayoutInflater inflater = activity.getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.dialog_admin_login, null);
		EditText password_view = dialogView.findViewById(R.id.password);

		builder.setView(dialogView)
				.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
				.setPositiveButton(
						"Login", (dialog, which) -> {
							if (password_view.getText().toString().equals("MDP")) {
								onSuccess.run();
								dialog.cancel();

								return;
							}

							Toast.makeText(activity, "Mauvais mot de passe", Toast.LENGTH_SHORT).show();
							dialog.cancel();
						}
				);

		builder.show();
	}
}
