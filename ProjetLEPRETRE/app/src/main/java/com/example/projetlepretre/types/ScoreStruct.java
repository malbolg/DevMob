package com.example.projetlepretre.types;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/** Representation of scores as a class */
public class ScoreStruct implements Serializable {
	public int max_value, value;
	public String category;

	public ScoreStruct(String category, int value, int max_value) {
		this.value = value;
		this.max_value = max_value;
		this.category = category;
	}

	@NonNull
	@Override
	public String toString() {
		if (value == -1)
			return "N/A";
		else return value + "/" + max_value;
	}

	private void readObject(ObjectInputStream stream)
			throws IOException, ClassNotFoundException {
		stream.defaultReadObject();

		this.category = stream.readUTF();
		this.value = stream.readInt();
		this.max_value = stream.readInt();
	}

	private void writeObject(ObjectOutputStream stream)
			throws IOException, ClassNotFoundException {
		stream.defaultWriteObject();

		stream.writeUTF(category);
		stream.writeInt(value);
		stream.writeInt(max_value);
	}

}
