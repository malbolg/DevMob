package com.example.projetlepretre.helpers;

import androidx.annotation.NonNull;

import com.example.projetlepretre.types.Question;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class to parse filestreams for importing forms as csv files
 */
public class RessourceParser {

	/**
	 * <p>this function parses the questions and answers from a csv filestream.</p>
	 *
	 * <p>Questions rows are marked with a Q as the first column, which are followed
	 * by answer rows that are marked with a A as the first column and a boolean denoting if the
	 * answer is a necessity for correct answers. it is then ended in the next row with a E as the
	 * first column. see <code>qcm01.txt</code> in raw folder for an example.</p>
	 *
	 * <p>Any filestream not respecting this format will throw a <code>IOException.</code></p>
	 *
	 * @param is The InputStreamReader
	 * @return an arraylist containing the questions (a form)
	 * @throws IOException if the format is incorrect.
	 */
	@NonNull
	public static ArrayList<Question> parse_form_from_csv(InputStreamReader is)
			throws IOException {
		ArrayList<Question> form = new ArrayList<>();
		BufferedReader bf = new BufferedReader(is);

		String ln;
		Question current_question = null;
		while ((ln = bf.readLine()) != null) {
			String[] split = ln.split(";");
			String line_type = split[0];

			switch (line_type) {
				case "Q": {
					current_question = new Question();
					current_question.set_question(split[1]);
					break;
				}

				case "A": {
					if (current_question == null)
						throw new IOException("Invalid file format, answer found to an non-existant question");

					current_question.add_answer(split[1], Boolean.parseBoolean(split[2]));
					break;
				}

				case "E": {
					if (current_question == null)
						throw new IOException("Invalid file format, end of question was found but no question was present");

					form.add(current_question);
					current_question = null;
				}
			}
		}
		
		bf.close();

		return form;
	}


	public static Map<String, String> parse_links(InputStreamReader inputStreamReader)
			throws IOException {
		Map<String, String> links = new HashMap<>();
		BufferedReader bf = new BufferedReader(inputStreamReader);

		String ln;
		while ((ln = bf.readLine()) != null) {
			String[] pair = ln.split(";");
			String category = pair[0];
			String filename = pair[1];

			links.put(category, filename);
		}

		bf.close();
		return links;
	}
}
