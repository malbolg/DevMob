package com.example.projetlepretre.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projetlepretre.R;
import com.example.projetlepretre.helpers.RessourceHandler;

import java.util.Map;

public class PreFormCreationActivity extends AppCompatActivity {
	private static Map<String, String> links;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_form_category);

		links = RessourceHandler.load_links(this);
	}

	public void onCancel(View unused) {
		finish();
	}

	public void onValidate(View unused) {
		String cat = ((EditText) findViewById(R.id.category_input)).getText().toString();

		links = RessourceHandler.load_links(this);

		if (links.containsKey(cat)) {
			Toast.makeText(this,
					"Ce questionnaire existe déjà.", Toast.LENGTH_SHORT
			).show();

			return;
		}

		startActivity(
				new Intent(this, FormCreationActivity.class)
						.putExtra("category_name", cat)
		);

		finish();
	}
}