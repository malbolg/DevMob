package com.example.projetlepretre.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projetlepretre.R;
import com.example.projetlepretre.helpers.RessourceHandler;
import com.example.projetlepretre.types.ScoreListSingleton;
import com.example.projetlepretre.types.ScoreStruct;

import java.io.IOException;

public class ScoreActivity extends AppCompatActivity {

	TableLayout parent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_score);
		setSupportActionBar(findViewById(R.id.toolbar));


		parent = findViewById(R.id.scores);

		ScoreListSingleton scores = ScoreListSingleton.getInstance();

		for (ScoreStruct scr : scores) {
			TableRow entry = new TableRow(this);
			parent.addView(entry);

			TextView category = new TextView(this);
			category.setText(scr.category);

			TextView score_text = new TextView(this);
			score_text.setText(scr.toString());

			entry.addView(category);
			entry.addView(score_text);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater()
				.inflate(R.menu.score_options, menu);

		return true;
	}

	public void onScoreReset(MenuItem unused) throws IOException {
		parent.removeAllViews();
		ScoreListSingleton.getInstance().clear();
		RessourceHandler.write_scores(this);
	}

	public void onReturn(View unused) {
		finish();
	}
}