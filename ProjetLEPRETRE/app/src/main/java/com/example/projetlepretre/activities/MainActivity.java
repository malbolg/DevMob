package com.example.projetlepretre.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projetlepretre.R;
import com.example.projetlepretre.dialogs.AdminLoginDialog;
import com.example.projetlepretre.helpers.RessourceHandler;
import com.example.projetlepretre.types.ScoreListSingleton;
import com.example.projetlepretre.types.ScoreStruct;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
	// this is the link between categories and their respective data file
	Map<String, String> QCM_links;
	ScoreListSingleton scores;

	private ListView listView;
	private ArrayAdapter<String> adapter;
	private final ActivityResultLauncher<Intent> mForm = registerForActivityResult(
			new ActivityResultContracts.StartActivityForResult(), this::onFormReturn
	);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setSupportActionBar(findViewById(R.id.toolbar));

		scores = ScoreListSingleton.getInstance();
		listView = findViewById(R.id.list);

		if (!RessourceHandler.has_file(this, RessourceHandler.CATEGORIES_FILE_NAME)) {
			// Only ever happens if the files from the raw folder have not been
			// parsed and serialized, which we do here
			RessourceHandler.import_default_forms(this);
		}

		QCM_links = RessourceHandler.load_links(this);
		load_scores();
		load_form_selection();
	}

	public void load_scores() {
		RessourceHandler.load_scores(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater()
				.inflate(R.menu.main_options, menu);

		return true;
	}

	private void load_form_selection() {
		ArrayList<String> names = new ArrayList<>(QCM_links.keySet());

		adapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_list_item_single_choice, names
		) {
			@Override
			public boolean isEnabled(int position) {
				if (listView.getChildAt(position) == null)
					return false;

				return listView.getChildAt(position).isEnabled();
			}
		};

		listView.setAdapter(adapter);
		listView.post(this::disable_completed_forms);
	}

	private void disable_completed_forms() {
		for (int i = 0; i < listView.getChildCount(); i++) {
			View listEntry = listView.getChildAt(i);
			listEntry.setEnabled(!scores.has(adapter.getItem(i)));
		}
	}

	public void onFormSelect(View unused) {
		int i = listView.getCheckedItemPosition();

		if (i == ListView.INVALID_POSITION) {
			Toast.makeText(this,
					"Vous n'avez pas sélectionner de questionnaire", Toast.LENGTH_SHORT
			).show();

			return;
		}

		Intent intent = new Intent(this, FormActivity.class)
				.putExtra("category", adapter.getItem(i))
				.putExtra("file", QCM_links.get(adapter.getItem(i)));

		mForm.launch(intent);
		uncheck_list(listView);
	}

	private void uncheck_list(ListView l) {
		for (int i = 0; i < l.getCount(); i++)
			l.setItemChecked(i, false);
	}

	/**
	 * @noinspection DataFlowIssue
	 */
	private void onFormReturn(ActivityResult res) {
		if (res.getResultCode() != RESULT_OK)
			return;

		Intent intent = res.getData();

		ScoreStruct scr = (ScoreStruct) intent.getSerializableExtra("score");

		// safety to avoid duplicates
		scores.removeIf(
				score -> score.category.equals(scr.category)
		);

		scores.add(scr);
		disable_completed_forms();

		try {
			RessourceHandler.write_scores(this);
		} catch (IOException e) {
			Toast.makeText(this, "score write failed", Toast.LENGTH_SHORT).show();
		}
	}

	public void viewScores(MenuItem unused) {
		Intent intent = new Intent(this, ScoreActivity.class);
		startActivity(intent);
	}

	public void onScoreReset(MenuItem unused) throws IOException {
		ScoreListSingleton.getInstance().clear();
		RessourceHandler.write_scores(this);
		disable_completed_forms();
	}

	public void onFormCreation(MenuItem item) {
		AdminLoginDialog.show(this, () -> {
			Intent intent = new Intent(this, PreFormCreationActivity.class);
			startActivity(intent);
		});
	}
}