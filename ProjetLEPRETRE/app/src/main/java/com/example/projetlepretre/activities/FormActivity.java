package com.example.projetlepretre.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projetlepretre.R;
import com.example.projetlepretre.helpers.RessourceHandler;
import com.example.projetlepretre.types.FormIterator;
import com.example.projetlepretre.types.Question;
import com.example.projetlepretre.types.ScoreStruct;

import java.io.IOException;
import java.util.ArrayList;

public class FormActivity extends AppCompatActivity {
	FormIterator quest_iterator;

	ScoreStruct score;
	private TextView QuestionHeader, Counter;
	private ListView AnswerList;
	private ArrayAdapter<String> AnswerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_question);
		setSupportActionBar(findViewById(R.id.toolbar));

		Intent intent = getIntent();
		String cat = intent.getStringExtra("category");
		TextView tv = findViewById(R.id.category);
		QuestionHeader = findViewById(R.id.question);
		Counter = findViewById(R.id.counter);
		AnswerList = findViewById(R.id.answers);
		AnswerAdapter = new ArrayAdapter<>(this,
				android.R.layout.simple_list_item_single_choice, new ArrayList<>()
		);
		AnswerList.setAdapter(AnswerAdapter);
		tv.setText(cat);

		// Load questions from filename in intent and render the first one
		load_questions(intent);
		render_current_question();


		score = new ScoreStruct(
				cat, 0, quest_iterator.count()
		);
	}

	private void load_questions(Intent intent) {
		try {
			ArrayList<Question> questions = RessourceHandler.load_form(
					this, intent.getStringExtra("file")
			);

			quest_iterator = new FormIterator(this, questions);
		} catch (IOException e) {throw new RuntimeException(e);}
	}

	private void render_current_question() {
		Question question = quest_iterator.get_question();

		// get the question and set the header accordingly
		QuestionHeader.setText(question.get_header());

		// get the current index of the question and format it, set the header accordingly
		Counter.setText(
				String.format(
						getString(R.string.counter_def),
						quest_iterator.current_index() + 1, quest_iterator.count()
				)
		);

		AnswerAdapter.clear();


		for (String ans: question.get_possible_answers())
			AnswerAdapter.add(ans);

		uncheck_list(AnswerList);
		AnswerAdapter.notifyDataSetChanged();
	}

	public void onQuestionAnswer(View unused) {
		ArrayList<Integer> selected_answers = new ArrayList<>();
		Question question = quest_iterator.get_question();

		int idx = 0;

		while (idx < AnswerAdapter.getCount()) {
			if (AnswerList.isItemChecked(idx))
				selected_answers.add(idx);

			idx++;
		}

		if (selected_answers.size() == 0) {
			Toast.makeText(
					this, "Vous n'avez pas sélectionné de QCM", Toast.LENGTH_SHORT
			).show();

			return;
		}

		score.value += question.answer(selected_answers) ? 1 : 0;

		if(quest_iterator.has_next()) {
			quest_iterator.next();
			render_current_question();
		} else {
			onFinish();
		}
	}

	private void uncheck_list(ListView l) {
		for (int i = 0; i < l.getCount(); i++)
			l.setItemChecked(i, false);
	}

	/**
	 * when there is no more question to answer, return the score for treatment.
	 */
	public void onFinish() {
		Toast.makeText(this, score.toString(), Toast.LENGTH_SHORT).show();

		Intent intent = getIntent().putExtra("score", score);
		setResult(RESULT_OK, intent);
		finish();
	}

	public void onCancel(View unused) {
		setResult(RESULT_CANCELED, null);
		finish();
	}
}