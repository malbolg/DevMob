package com.example.projetlepretre.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projetlepretre.R;
import com.example.projetlepretre.helpers.RessourceHandler;
import com.example.projetlepretre.types.Question;

import java.util.ArrayList;

public class FormCreationActivity extends AppCompatActivity {
	private ArrayList<Question> questions;
	private EditText question_header;
	private ArrayList<EditText> answer_fields;
	private LinearLayout correct_answer_selector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_form_creation);

		questions = new ArrayList<>();

		question_header = findViewById(R.id.question_header);
		generate_question_hints();

		// if need be we can add a button to add additional answers in the activity
		// hence the arraylist
		answer_fields = new ArrayList<>();
		answer_fields.add(findViewById(R.id.answer_1));
		answer_fields.add(findViewById(R.id.answer_2));
		answer_fields.add(findViewById(R.id.answer_3));

		correct_answer_selector = findViewById(R.id.correct_answer_selector);
	}

	private void generate_question_hints() {
		question_header.setHint(String.format(
				getString(R.string.question_def), questions.size() +1
		));
	}

	public void onReturn(View unused) {
		setResult(RESULT_CANCELED);
		finish();
	}

	public void onDone(View unused) {
		if (questions.size() < 1) {
			Toast.makeText(this,
					"Please input atleast 1 question", Toast.LENGTH_SHORT
			).show();

			return;
		}

		String cat = getIntent().getStringExtra("category_name");
		RessourceHandler.save_form(this, cat, questions);

		Toast.makeText(this,
				"Questionnaire crée, il apparaîtra au prochain démarrage", Toast.LENGTH_SHORT
		).show();

		finish();
	}

	public void onNextQuestion(View unused) {
		Question question = new Question();
		ArrayList<Integer> selected_answers = new ArrayList<>();

		//
		String question_txt = question_header.getText().toString();

		if (question_txt.equals("")) {
			Toast.makeText(this,
					"Remplissez l'énnoncé de la question", Toast.LENGTH_SHORT
			).show();

			return;
		}

		question.set_question(question_txt);
		//

		//
		for (int i = 0; i < correct_answer_selector.getChildCount(); i++) {
			CheckBox b = (CheckBox) correct_answer_selector.getChildAt(i);

			if (b.isChecked())
				selected_answers.add(i);
		}

		if (selected_answers.size() == 0) {
			Toast.makeText(this,
					"Vous devez selectionner une réponse valide", Toast.LENGTH_SHORT
			).show();

			return;
		}
		//

		//
		for (int answer_idx = 0; answer_idx < answer_fields.size(); answer_idx++) {
			String answer_txt = answer_fields.get(answer_idx).getText().toString();

			if (answer_txt.equals("")) {
				Toast.makeText(this,
						"Vous devez remplir toutes les réponses", Toast.LENGTH_SHORT
				).show();

				return;
			}

			question.add_answer(answer_txt, selected_answers.contains(answer_idx));
		}
		//

		questions.add(question);

		clear_all();
		generate_question_hints();
	}

	private void clear_all() {
		for (int idx = 0; idx < answer_fields.size(); idx++) {
			question_header.setText("");
			answer_fields.get(idx).setText("");

			CheckBox check = (CheckBox) correct_answer_selector.getChildAt(idx);
			check.setChecked(false);
		}
	}
}