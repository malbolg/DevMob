package com.example.tp05_exercice1;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {
	private final ActivityResultLauncher<String> mGetImage = registerForActivityResult(
			new ActivityResultContracts.GetContent(), this::onActivityResult
	);

	private TextView UriText;
	private ImageView renderer;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		UriText = findViewById(R.id.URI);
		renderer = findViewById(R.id.img_render);
	}

	private void ChargerImage(Uri ImageURI) throws FileNotFoundException {
		BitmapFactory.Options option = new BitmapFactory.Options();
		option.inMutable = true;

		Bitmap bm = BitmapFactory.decodeStream(
				getContentResolver().openInputStream(ImageURI),
				null, option
		);

		renderer.setImageBitmap(bm);
	}

	public void onUploadAttempt(View unused) {
		mGetImage.launch("image/*");
	}

	private void onActivityResult(Uri uri) {
		if (uri == null)
			return;

		UriText.setText(uri.toString());

		try {
			ChargerImage(uri);
		} catch (FileNotFoundException e) {
			return;
		}
	}
}