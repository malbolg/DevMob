package com.example.tp05_exercice2;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

public class BitmapManipulator {
	public static void flipBitmapX(Bitmap bm) {
		final int width = bm.getWidth();
		final int height = bm.getHeight();
		final int half_width = width / 2;

		for (int offset_left = 0; offset_left < half_width; offset_left++) {
			for (int offset_y = 0; offset_y < height; offset_y++) {
				int offset_right = width - offset_left - 1;

				int right_color = bm.getPixel(offset_right, offset_y);
				int left_color = bm.getPixel(offset_left, offset_y);

				bm.setPixel(offset_left, offset_y, right_color);
				bm.setPixel(offset_right, offset_y, left_color);
			}
		}
	}

	public static void flipBitmapY(Bitmap bm) {
		final int width = bm.getWidth();
		final int height = bm.getHeight();
		final int half_height = width / 2;

		for (int offset_x = 0; offset_x < width; offset_x++) {
			for (int offset_top = 0; offset_top < half_height; offset_top++) {
				int offset_bottom = height - offset_top - 1;

				int top_color = bm.getPixel(offset_x, offset_top);
				int bottom_color = bm.getPixel(offset_x, offset_bottom);

				bm.setPixel(offset_x, offset_top, bottom_color);
				bm.setPixel(offset_x, offset_bottom, top_color);
			}
		}
	}

	public static void applyGreyscale(Bitmap bm) {
		final int width = bm.getWidth();
		final int height = bm.getHeight();

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int clr = bm.getPixel(x, y);

				int r = Color.red(clr);
				int g = Color.green(clr);
				int b = Color.blue(clr);

				int m = (r + g + b) / 3;
				bm.setPixel(x, y, Color.rgb(m, m, m));
			}
		}
	}


	public static void applyInverse(Bitmap bm) {
		final int width = bm.getWidth();
		final int height = bm.getHeight();

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int clr = bm.getPixel(x, y);

				int r = Color.red(clr);
				int g = Color.green(clr);
				int b = Color.blue(clr);

				bm.setPixel(x, y, Color.rgb(255 - r, 255 - g, 255 - b));
			}
		}
	}

	/** @noinspection SuspiciousNameCombination*/
	public static Bitmap rotateAntiClockwise(Bitmap prev_bm) {
		int width = prev_bm.getWidth();
		int height = prev_bm.getHeight();
		Bitmap bm = Bitmap.createBitmap(height, width, prev_bm.getConfig());

		for( int x = 0; x < width; x++ ) {
			for( int y = 0; y < height; y++ ) {
				int target_color = prev_bm.getPixel(width - 1 - x, y);

				bm.setPixel(y, x, target_color);
			}
		}

		return bm;
	}

	/** @noinspection SuspiciousNameCombination*/
	public static Bitmap rotateClockwise(Bitmap prev_bm) {
		int width = prev_bm.getWidth();
		int height = prev_bm.getHeight();
		Bitmap bm = Bitmap.createBitmap(height, width, prev_bm.getConfig());

		for( int x = 0; x < width; x++ ) {
			for( int y = 0; y < height; y++ ) {
				int target_color = prev_bm.getPixel(x, height - 1 - y);

				bm.setPixel(y, x, target_color);
			}
		}

		return bm;
	}

	public static Bitmap cloneBitmap(Bitmap bitmap) {
		return bitmap.copy(bitmap.getConfig(), true);
	}
}
