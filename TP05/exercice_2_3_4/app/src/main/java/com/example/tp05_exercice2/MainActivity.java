package com.example.tp05_exercice2;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {
	private final ActivityResultLauncher<String> mGetImage = registerForActivityResult(
			new ActivityResultContracts.GetContent(), this::onUriReceived
	);

	private TextView UriText;
	private ImageView renderer;

	private boolean hasImage;

	Bitmap init_bitmap;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		setSupportActionBar(findViewById(R.id.toolbar));

		hasImage = false;
		UriText = findViewById(R.id.URI);
		renderer = findViewById(R.id.img_render);

		registerForContextMenu(renderer);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater()
				.inflate(R.menu.main, menu);

		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		getMenuInflater()
				.inflate(R.menu.context, menu);
	}

	private void ChargerImage(Uri ImageURI) throws FileNotFoundException {
		BitmapFactory.Options option = new BitmapFactory.Options();
		option.inMutable = true;

		Bitmap bm = BitmapFactory.decodeStream(
				getContentResolver().openInputStream(ImageURI),
				null, option
		);

		renderer.setImageBitmap(bm);
	}

	public void onUpload(View unused) {
		mGetImage.launch("image/*");
	}

	private void onUriReceived(Uri uri) {
		if (uri == null)
			return;

		UriText.setText(uri.toString());

		try {
			ChargerImage(uri);
			backup_bitmap();
			hasImage = true;
		} catch (FileNotFoundException e) {
			hasImage = false;
		}
	}

	private void backup_bitmap() {
		init_bitmap = BitmapManipulator.cloneBitmap(getBitmap());
	}

	public void OnMirrorHorizontally(MenuItem unused) {
		if (!hasImage) return;

		BitmapManipulator.flipBitmapX(getBitmap());
	}

	public void onMirrorVertically(MenuItem unused) {
		if (!hasImage) return;

		BitmapManipulator.flipBitmapY(getBitmap());
	}

	public void onGreyscale(MenuItem unused) {
		if (!hasImage) return;

		BitmapManipulator.applyGreyscale(getBitmap());
	}

	public void onInvert(MenuItem unused) {
		if (!hasImage) return;

		BitmapManipulator.applyInverse(getBitmap());
	}

	public void onRotateClockwise(MenuItem unused) {
		if (!hasImage) return;

		renderer.setImageBitmap(
				BitmapManipulator.rotateClockwise(getBitmap())
		);
	}

	public void onRotateAntiClockwise(MenuItem unused) {
		if (!hasImage) return;

		renderer.setImageBitmap(
				BitmapManipulator.rotateAntiClockwise(getBitmap())
		);
	}

	public void onRestore(View unused) {
		if (!hasImage) return;

		renderer.setImageBitmap(
				BitmapManipulator.cloneBitmap(init_bitmap)
		);
	}

	public Bitmap getBitmap() {
		return ((BitmapDrawable) renderer.getDrawable()).getBitmap();
	}
}