package com.example.contentproviderlepretre;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
	private SwitchCompat switcher;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		switcher = findViewById(R.id.switcher);
	}


	@SuppressLint("Range")
	public void onTest(View unused) {
		startActivity(
				new Intent(this, ListActivity.class)
						.putExtra("order", switcher.isChecked())
		);
	}
}