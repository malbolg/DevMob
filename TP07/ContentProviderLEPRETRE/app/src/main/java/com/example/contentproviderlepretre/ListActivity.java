package com.example.contentproviderlepretre;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
	private ListView listView;
	private SwitchCompat switcher;
	private ArrayAdapter<String> adapter;

	private ActivityResultLauncher<Intent> act_add = registerForActivityResult(
			new ActivityResultContracts.StartActivityForResult(), this::onAddReturn
	);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		switcher = findViewById(R.id.switcher);
		listView = findViewById(R.id.list);
		adapter = new ArrayAdapter<>(
				this, android.R.layout.simple_list_item_multiple_choice,
				new ArrayList<>()
		);

		boolean order = getIntent().getBooleanExtra("order", false);

		switcher.setChecked(order);
		listView.setAdapter(adapter);
		updateWords(order);
	}

	private void updateWords(boolean order) {
		Cursor csr = getContentResolver().query(
				UserDictionary.Words.CONTENT_URI,
				new String[]{UserDictionary.Words.WORD},
				null, null,
				UserDictionary.Words.WORD + " " + (order ? "DESC" : "ASC")
		);

		if (csr == null) {
			Toast.makeText(this, "Failed to query dictionnary", Toast.LENGTH_SHORT)
					.show();

			return;
		}

		adapter.clear();

		if( csr.getCount() == 0 || !csr.moveToFirst() ) {
			csr.close();
			return;
		}

		do {
			@SuppressLint("Range")
			String str = csr.getString(
					csr.getColumnIndex(UserDictionary.Words.WORD)
			);

			Log.i("i", str);
			adapter.add(str);
		} while (csr.moveToNext());
		csr.close();

		adapter.notifyDataSetChanged();
	}

	public void onOrderSwitch(View unused) {
		updateWords(switcher.isChecked());
	}

	public void onAdd(View unused) {
		act_add.launch(
				new Intent(this, addActivity.class)
		);

		updateWords(switcher.isChecked());
	}

	public void onAddReturn(ActivityResult actRes) {
		if (actRes.getResultCode() != RESULT_OK)
			return;

		updateWords(switcher.isChecked());
	}

	public void onRemove(View unused) {
		ContentResolver c = getContentResolver();

		for (int i = 0; i < listView.getCount(); i++) {
			if (listView.isItemChecked(i))
				// j'ai eu des difficultés a faire un seul appel...
				c.delete(
						UserDictionary.Words.CONTENT_URI,
						UserDictionary.Words.WORD + " = ?",
						new String[]{adapter.getItem(i)}
				);

			listView.setItemChecked(i, false);
		}

		updateWords(switcher.isChecked());
	}
}