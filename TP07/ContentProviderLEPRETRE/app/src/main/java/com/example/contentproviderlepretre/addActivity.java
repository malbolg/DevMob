package com.example.contentproviderlepretre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.view.View;
import android.widget.EditText;

public class addActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add);
	}


	public void onFinish(View unused) {
		String str = ((EditText)findViewById(R.id.text_content))
				.getText()
				.toString();

		ContentValues content = new ContentValues();
		content.put(UserDictionary.Words.WORD, str);

		if (exists(str)) {
			setResult(RESULT_CANCELED, null);
		} else {
			getContentResolver().insert(
					UserDictionary.Words.CONTENT_URI, content
			);

			setResult(RESULT_OK, null);
		}

		finish();
	}

	public boolean exists(String str) {
		Cursor csr = getContentResolver().query(
				UserDictionary.Words.CONTENT_URI,
				new String[]{UserDictionary.Words._ID},
				UserDictionary.Words.WORD + " = ?",
				new String[]{str}, null
		);

		int count = csr.getCount();
		csr.close();

		return count > 0;
	}
}