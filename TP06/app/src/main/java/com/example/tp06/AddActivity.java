package com.example.tp06;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class AddActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add);
	}

	public void onConfirm(View unused) {
		Intent response = new Intent();

		String text = ((TextView) findViewById(R.id.text))
				.getText()
				.toString();


		response.putExtra("result", text);
		setResult(RESULT_OK, response);

		finish();
	}

	public void onCancel(View unused) {
		setResult(RESULT_CANCELED, null);
		finish();
	}
}