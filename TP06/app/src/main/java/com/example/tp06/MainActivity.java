package com.example.tp06;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

	private final ActivityResultLauncher<Intent> tryAddContract = registerForActivityResult(
			new ActivityResultContracts.StartActivityForResult(), this::onResult
	);

	private ArrayAdapter<String> ArrAdapt;
	private ListView listView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setSupportActionBar(findViewById(R.id.toolbar));

		listView = findViewById(R.id.list);

		onLoad();

		listView.setAdapter(ArrAdapt);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		getMenuInflater()
				.inflate(R.menu.action_menu, menu);

		return true;
	}

	public void onAddAction(View unused) {
		Intent intent = new Intent(this, AddActivity.class);

		tryAddContract.launch(intent);
	}

	public void onResult(ActivityResult activityResult) {
		if (activityResult.getResultCode() != RESULT_OK)
			return;

		Intent res = activityResult.getData();
		assert res != null;

		String added = res.getStringExtra("result");

		Toast.makeText(this, "added \"" + added + "\"", Toast.LENGTH_SHORT)
				.show();

		ArrAdapt.add(added);
	}

	public void doDelete(MenuItem unused) {
		for (int i = ArrAdapt.getCount(); i >= 0; i--) {
			if (listView.isItemChecked(i))
				ArrAdapt.remove(ArrAdapt.getItem(i));
		}

		listView.clearChoices();
		ArrAdapt.notifyDataSetChanged();
	}

	public void onSave(MenuItem unused) {
		try (FileOutputStream fileOutputStream = openFileOutput(
				"saved_tasks.csv", MODE_PRIVATE
		)) {
			OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);


			for (int i = 0; i < ArrAdapt.getCount(); i++) {
				writer.write(ArrAdapt.getItem(i));
				writer.write("\n");
			}

			Toast.makeText(this, "saved", Toast.LENGTH_SHORT).show();
			writer.close();
		} catch (IOException e) {
			Toast.makeText(this, "failed to save", Toast.LENGTH_SHORT).show();
		}
	}

	public void onLoad() {
		try (FileInputStream fstream = openFileInput("saved_tasks.csv")) {
			InputStreamReader inputStreamReader = new InputStreamReader(fstream);
			BufferedReader reader = new BufferedReader(inputStreamReader);
			ArrayList<String> tasks = new ArrayList<>();

			String ln;
			while( (ln = reader.readLine()) != null ) {
				tasks.add(ln);
				Log.d("d", "AZERTY");
			}

			ArrAdapt = new ArrayAdapter<>(
					this, android.R.layout.simple_list_item_checked, tasks
			);

			reader.close();
			inputStreamReader.close();
		} catch (IOException test) {
			Log.d("d", test.toString());
			ArrayList<String> tasks = new ArrayList<>(
					Arrays.asList(
							"Test1", "Test1", "Test3", "Test4", "Test5",
							"Test6", "Test7", "Test8", "Test8", "Test9",
							"Test10", "Test11", "Test12", "Test13", "Test14"
					)
			);

			ArrAdapt = new ArrayAdapter<>(
					this, android.R.layout.simple_list_item_checked, tasks
			);
		}
	}
}