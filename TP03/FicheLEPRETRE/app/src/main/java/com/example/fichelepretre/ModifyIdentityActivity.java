package com.example.fichelepretre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.HashMap;
import java.util.Map;

public class ModifyIdentityActivity extends AppCompatActivity {
	private Map<String, EditText> Identity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_identity);

		Intent context = getIntent();
		Identity = new HashMap<>();
		Identity.put("name", (EditText) findViewById(R.id.nameValue));
		Identity.put("fname", (EditText) findViewById(R.id.fnameValue));
		Identity.put("tel", (EditText) findViewById(R.id.telValue));

		for( String keystr : Identity.keySet() ) {
			String currentStr = context.getStringExtra(keystr);

//						should not happen, otherwise something went horribly wrong
//			if( currentStr == null ) continue;
//			if( !Address.containsKey(keystr) ) continue;

			//noinspection DataFlowIssue
			if( currentStr.equals("") ) continue;

			//noinspection DataFlowIssue
			Identity.get(keystr).setText(currentStr);
		}
	}

	public void onSubmit(View unused) {
		Intent result= getIntent();

		putValuesFromMapInContext(result, Identity);

		setResult(RESULT_OK, result);

		finish();
	}

	public void onCancel(View unused) {
		setResult(RESULT_CANCELED, null);
		finish();
	}

	private void putValuesFromMapInContext(Intent context, Map<String, EditText> Pairs) {
		for (Map.Entry<String, EditText> entrySet : Pairs.entrySet()) {
			context.putExtra(
					entrySet.getKey(),
					entrySet.getValue().getText().toString()
			);
		}
	}
}