package com.example.fichelepretre;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

	private Map<String, TextView> Identity;
	private Map<String, TextView> Address;

	private ActivityResultLauncher<Intent> mGetAddressModification = registerForActivityResult(
			new ActivityResultContracts.StartActivityForResult(),

			new ActivityResultCallback<ActivityResult>() {
				@Override
				public void onActivityResult(ActivityResult o) {
					if (o.getResultCode() != RESULT_OK) return;

					Intent res = o.getData();
					assert res != null;

					setValuesFromIntentResultToMap(res, Address);
				}
			}
	);

	private ActivityResultLauncher<Intent> mGetIdentityModification = registerForActivityResult(
			new ActivityResultContracts.StartActivityForResult(),

			new ActivityResultCallback<ActivityResult>() {
				@Override
				public void onActivityResult(ActivityResult o) {
					if (o.getResultCode() != RESULT_OK) return;

					Intent res = o.getData();
					assert res != null;

					setValuesFromIntentResultToMap(res, Identity);
				}
			}
	);


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Identity = new HashMap<>();
		Address = new HashMap<>();

		Identity.put("name", (TextView) findViewById(R.id.nameValue));
		Identity.put("fname", (TextView) findViewById(R.id.fnameValue));
		Identity.put("tel", (TextView) findViewById(R.id.telValue));

		Address.put("num", (TextView) findViewById(R.id.numValue));
		Address.put("way", (TextView) findViewById(R.id.wayValue));
		Address.put("postal", (TextView) findViewById(R.id.postalValue));
		Address.put("city", (TextView) findViewById(R.id.cityValue));
	}

	private void putValuesFromMapInContext(Intent context, Map<String, TextView> Pairs) {
		for (Map.Entry<String, TextView> entrySet : Pairs.entrySet()) {
			context.putExtra(
					entrySet.getKey(),
					entrySet.getValue().getText().toString()
			);
		}
	}

	private void setValuesFromIntentResultToMap(Intent context, Map<String, TextView> Pairs) {
		for (String keyStr : Pairs.keySet()) {
			String newStrValue = context.getStringExtra(keyStr);

			if (newStrValue != null) {
				//no need to check for null (superfluous)
				//noinspection DataFlowIssue
				Pairs.get(keyStr).setText(newStrValue);
			}
		}
	}

	public void onModifyAddress(View unused) {
		Intent context = new Intent(this, ModifyAddressActivity.class);

		putValuesFromMapInContext(context, Address);

		mGetAddressModification.launch(context);
	}

	public void onModifyIdentity(View unused) {
		Intent context = new Intent(this, ModifyIdentityActivity.class);

		putValuesFromMapInContext(context, Identity);

		mGetIdentityModification.launch(context);
	}

}