package com.example.fichelepretre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.HashMap;
import java.util.Map;

public class ModifyAddressActivity extends AppCompatActivity {
	private Map<String, EditText> Address;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_modify_address);

		Intent context = getIntent();

		Address = new HashMap<>();
		Address.put("num", (EditText) findViewById(R.id.numValue));
		Address.put("way", (EditText) findViewById(R.id.wayValue));
		Address.put("postal", (EditText) findViewById(R.id.postalValue));
		Address.put("city", (EditText) findViewById(R.id.cityValue));

		for (String keystr : Address.keySet()) {
			String currentStr = context.getStringExtra(keystr);
//						should not happen, otherwise something went horribly wrong
//			if( currentStr == null ) continue;
//			if( !Address.containsKey(keystr) ) continue;

			//noinspection DataFlowIssue
			if (currentStr.equals("")) continue;

			//noinspection DataFlowIssue
			Address.get(keystr).setText(currentStr);
		}
	}

	public void onSubmit(View unused) {
		Intent result= getIntent();

		putValuesFromMapInContext(result, Address);

		setResult(RESULT_OK, result);

		finish();
	}

	public void onCancel(View unused) {
		setResult(RESULT_CANCELED, null);
		finish();
	}

	private void putValuesFromMapInContext(Intent context, Map<String, EditText> Pairs) {
		for (Map.Entry<String, EditText> entrySet : Pairs.entrySet()) {
			context.putExtra(
					entrySet.getKey(),
					entrySet.getValue().getText().toString()
			);
		}
	}
}