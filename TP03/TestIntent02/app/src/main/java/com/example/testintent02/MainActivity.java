package com.example.testintent02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
	TextView textWrapper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		textWrapper = findViewById(R.id.data);
	}

	public void OnMod(View unused) {
		String str = textWrapper.getText().toString();

		Intent intent = new Intent(this, SubActivity.class)
				.putExtra("str", str);

		startActivityForResult(intent, 0);
	}

	@Override
	protected void onActivityResult(int ReqCode, int ResCode, Intent dat) {
		super.onActivityResult(ReqCode, ResCode, dat);

		if (ResCode != RESULT_OK)
			return;

		switch (ReqCode) {
			case 0:
				changeTextIfNotNull(
						dat.getStringExtra("str")
				);
			break;

			default:
				return;
		}
	}

	public void changeTextIfNotNull(String str) {
		if (str == null || str.equals(""))
			return;

		textWrapper.setText(str);
	}
}