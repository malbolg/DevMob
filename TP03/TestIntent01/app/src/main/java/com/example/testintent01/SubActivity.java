package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SubActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        Intent intention = getIntent();
        String str = intention.getStringExtra("str");

        if( str == null || str.equals("") )
            return;

        TextView T = findViewById(R.id.target);
        T.setText(str);
    }
}