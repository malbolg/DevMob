package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText textWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textWrapper = findViewById(R.id.data);
    }

    public void OnSend(View unused) {
        String str = textWrapper.getText().toString();

        Intent intent = new Intent(this, SubActivity.class)
                .putExtra("str", str);

        startActivity(intent);
    }
}