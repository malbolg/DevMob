package com.example.testintent03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
	TextView textWrapper;
	TextView PreviewWrapper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		textWrapper = findViewById(R.id.data);
		PreviewWrapper = findViewById(R.id.textEditValue);
	}

	public void OnMod(View unused) {
		String str = textWrapper.getText().toString();

		Intent intent = new Intent(this, SubActivity.class)
				.putExtra("str", str);

		startActivityForResult(intent, 0);
	}

	public void OnApply(View unused) {
		textWrapper.setText(PreviewWrapper.getText().toString());
	}

	@Override
	protected void onActivityResult(int ReqCode, int ResCode, Intent dat) {
		super.onActivityResult(ReqCode, ResCode, dat);

		if (ResCode != RESULT_OK)
			return;

		switch (ReqCode) {
			case 0:
				changePreviewIfNotNull(
						dat.getStringExtra("str")
				);
				break;

			default: return;
		}
	}

	public void changePreviewIfNotNull(String str) {
		if( str == null || str.equals("") )
			return;

		PreviewWrapper.setText(str);
	}
}