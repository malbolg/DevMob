package com.example.testintent03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SubActivity extends AppCompatActivity {
	EditText TextWrapper;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sub);
		TextWrapper = findViewById(R.id.textEdit);

		Intent intention = getIntent();
		String str = intention.getStringExtra("str");

		if( str == null || str.equals("") )
			return;

		TextWrapper.setText(str);
	}

	public void OnInvert(View unused) {
		String str = TextWrapper.getText().toString();

		Intent res = new Intent().putExtra("str", inverseString(str));

		setResult(RESULT_OK, res);
		finish();
	}

	public void OnMaj(View unused) {
		String str = TextWrapper.getText().toString();

		Intent res = new Intent().putExtra("str", str.toUpperCase());

		setResult(RESULT_OK, res);
		finish();
	}

	private String inverseString(String str) {
		StringBuilder strbd = new StringBuilder();
		strbd.append(str);
		strbd.reverse();
		return strbd.toString();
	}

}
