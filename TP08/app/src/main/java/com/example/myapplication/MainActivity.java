package com.example.myapplication;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.exifinterface.media.ExifInterface;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
	private final ActivityResultLauncher<String> mGetImageByUri = registerForActivityResult(
			new ActivityResultContracts.GetContent(), this::doInsertByURI
	);

	@RequiresApi(api = Build.VERSION_CODES.Q)
	private final ActivityResultLauncher<Intent> mGetImageByIntent = registerForActivityResult(
			new ActivityResultContracts.StartActivityForResult(), this::doInsertByIntent
	);

	private final ActivityResultLauncher<String> requestPermissionLauncher = registerForActivityResult(
			new ActivityResultContracts.RequestPermission(), isGranted -> {
				if (!isGranted) {
					Toast.makeText(this, "missing permissions", Toast.LENGTH_SHORT).show();
				}
			}
	);


	private ImageView renderer;
	private TextView Lat, Long;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		renderer = findViewById(R.id.renderer);
		Lat = findViewById(R.id.Latitude);
		Long = findViewById(R.id.Longitude);
	}

	public void launchInsert(View unused) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
			mGetImageByUri.launch("image/*");
			return;
		}

		mGetImageByIntent.launch(
				new Intent(Intent.ACTION_PICK).setType("image/*")
		);
	}

	private void doInsertByURI(Uri uri) {
		if (uri == null)
			return;


		do_update(uri);
	}

	@RequiresApi(api = Build.VERSION_CODES.Q)
	private void doInsertByIntent(ActivityResult actResult) {
		if (	actResult.getResultCode() != RESULT_OK
				|| actResult.getData() == null
				|| actResult.getData().getData() == null)
			return;

		Uri uri = actResult.getData().getData();
		do_update(uri);
	}

	private void do_update(Uri ImageURI) {
		try (InputStream image_stream =
					 getContentResolver().openInputStream(ImageURI)
		) {
			updateImage(image_stream);
			updateExif(image_stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void updateImage(InputStream is) {
		BitmapFactory.Options option = new BitmapFactory.Options();
		option.inMutable = true;

		Bitmap bm = BitmapFactory.decodeStream(
				is, null, option
		);

		renderer.setImageBitmap(bm);
	}

	private void updateExif(InputStream is) throws IOException {
		ExifInterface exifInterface = new ExifInterface(is);

		String str1 = "Latitude : " + exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
		Lat.setText(str1);

		String str2 = "Longitude : " + exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
		Long.setText(str2);
	}

	@RequiresApi(api = Build.VERSION_CODES.Q)
	private boolean doPermissions() {
		if(checkSelfPermission(Manifest.permission.ACCESS_MEDIA_LOCATION) == PackageManager.PERMISSION_GRANTED)
			return true;

		requestPermissionLauncher.launch(Manifest.permission.ACCESS_MEDIA_LOCATION);

		return checkSelfPermission(Manifest.permission.ACCESS_MEDIA_LOCATION) == PackageManager.PERMISSION_GRANTED;
	}
}