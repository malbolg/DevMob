package com.example.tp4lepretre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class CallActivity extends AppCompatActivity {

	private EditText Number;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_call);

		Number = (EditText) findViewById(R.id.numValue);
	}


	public void onSubmit(View view) {
		startActivity(
				new Intent(
					Intent.ACTION_DIAL,
					Uri.parse("tel:" + getNumber())
				)
		);
	}

	public void onCancel(View view) {
		finish();
	}

	private String getNumber() {
		return Number.getText().toString();
	}
}