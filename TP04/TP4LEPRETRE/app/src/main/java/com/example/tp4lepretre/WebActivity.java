package com.example.tp4lepretre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class WebActivity extends AppCompatActivity {
	private EditText Link;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web);

		Link = findViewById(R.id.linkValue);
	}

	public void onSubmit(View view) {
		startActivity(
				new Intent(
						Intent.ACTION_WEB_SEARCH,
						Uri.parse(getLink())
				)
		);
	}

	public void onCancel(View view) {
		finish();
	}

	private String getLink() {
		return Link.getText().toString();
	}
}