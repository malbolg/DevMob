package com.example.tp4lepretre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

	Map<Integer, String> ViewToId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ViewToId = new HashMap<>();

	}

	public void onAction(View v) {
		Button btn = (Button) v;
		int id = btn.getId();

		Toast
			.makeText(this, btn.getText().toString(), Toast.LENGTH_SHORT)
			.show();

		if		(id == R.id.call)	onCall();
		else if (id == R.id.sms)	onMessage();
		else if (id == R.id.mms)	onMessage();
		else if (id == R.id.web)	onWeb();
		else if (id == R.id.geo)	onGeo();
	}

	public void onCall() {
		startActivity(
				new Intent(this, CallActivity.class)
		);
	}

	public void onMessage() {
		startActivity(
				new Intent(this, MessageActivity.class)
		);
	}
	public void onWeb() {
		startActivity(
				new Intent(this, WebActivity.class)
		);
	}
	public void onGeo() {
		Intent intent = new Intent(
				Intent.ACTION_VIEW,
				Uri.parse("geo:0,0?q=1600+Amphitheatre+Parkway,+Mountain+View,+CA+94043")
		);

		startActivity(intent);
	}
}