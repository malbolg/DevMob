package com.example.tp4lepretre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MessageActivity extends AppCompatActivity {
	private EditText Number;
	private EditText Message;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_msgactivity);

		Number = findViewById(R.id.numValue);
		Message = findViewById(R.id.msgValue);
	}

	public void onSubmit(View view) {
		Intent intent = new Intent(
				Intent.ACTION_SENDTO,
				Uri.parse("sms:" + getNumber())
		);

		intent.putExtra("sms_body", getContents());

		startActivity(intent);
	}

	public void onCancel(View view) {
		finish();
	}


	private String getNumber() {
		return Number.getText().toString();
	}

	private String getContents() {
		return Message.getText().toString();
	}
}